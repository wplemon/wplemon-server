<?php

acf_add_options_page(
    array(
        'page_title'      => 'WPLEMON Server',
        'menu_slug'       => 'wplemon-server',
        'capability'      => 'manage_options',
        'position'        => false,
        'parent_slug'     => '',
        'icon_url'        => 'dashicons-networking',
        'redirect'        => true,
        'post_id'         => 'options',
        'autoload'        => true,
        'update_button'   => 'Update',
        'updated_message' => 'Options Updated',
    )
);
