<?php
/**
 * Plugin Name:   wplemon-Server
 * Plugin URI:    https://wplemon.com
 * Description:   Handles additional requests to the wplemon Server.
 * Author:        Aristeides Stathopoulos
 * Author URI:    https://aristath.github.io
 * Version:       1.0
 * Text Domain:   wplemon-server
 */

add_action( 'plugins_loaded', function() {
    if ( class_exists( 'ACF' ) ) {
        include_once plugin_dir_path( __FILE__ ) . 'admin-page.php';
        include_once plugin_dir_path( __FILE__ ) . 'actions.php';
    }
});